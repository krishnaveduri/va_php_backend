
$(document).ready(function(){
    $('.owl-one').owlCarousel({
    
		autoPlay: 10000, 
		loop: true,
		margin:10,
		center: false,
		video:true,
		videoWidth: false,
        merge:true,
		rewind: true,
		dots: false,
		nav: true,
		autoplay: false,
		navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
		items: 4,
		itemsDesktop: [1199,4],
		itemsDesktopSmall: [979, 2],
		responsiveClass: true,
		responsive: {
			0: {
				items: 1,
				nav: true
			},
			600: {
				items: 2,
				nav: true
			},
			1000: {
				items: 4,
				nav: true
			}
		}

    });

    $('.owl-two').owlCarousel({
    
		autoPlay: 10000, 
		loop: true,
		margin:10,
		center: true,
        
		rewind: true,
		dots: false,
		nav: true,
		autoplay: false,
		navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
		items: 6,
		itemsDesktop: [1199,6],
		itemsDesktopSmall: [979, 4],
		responsiveClass: true,
		responsive: {
			0: {
				items: 1,
				nav: true
			},
			600: {
				items: 4,
				nav: true
			},
			1000: {
				items: 6,
				nav: true
			}
		}

	});
	
});

