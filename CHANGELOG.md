# Changelog

All notable changes to this project will be documented in this file.

##[1.2.1] - 


## 07-01-2018 - ADMIN 

- Redeems Request Payment - handled the status change based on the admin paid amount.

- View user details redeems help added

- Banner ads - usage note added


## 06-01-2018 - ADMIN 

- channel user_id and channel_id save issue fixed

## 04.01.2018 - Admin 

- user type added in users list and view page

- user wishlist and history - username added

- View Videos - Channel link added


## 02.01.2018 - Admin 

- Revenues dashboard and PPV payments sections added 

## 01.01.2018 - user 

- PPV - commission spilit fixed in stripe and paypal 

- Header - Profile options order changed

## 01.01.2018 - Admin 

- Dashboard - total revenue changes based on the subscription and ppv payments

- Profile - email exists issue fixed 


## 30.12.2017 - Admin 

- Nav bar - realigned options 

- Settings - Realigned options 


## 30.12.2017 - User 

- User side, - Added Subscription History.

- User side, Added PPV History.


##[1.2.0] - 25 Dec 2017

### Fixed 

- mychannel hide added based on the create_channel_by_user fetaure

## [1.0.0] - 2017-12-03

### Fixed


2016-11-02 - smtp confiure error page redirection fixed 

### Fixed

- Twitter login issue fixed.

### Fixed

- Commission Spilit Issue fixed.

### Changed 

- c3f6bdb , Embed Link UI Changes

### Added

- ee34518 , 4f57bb5 -BroadCast By User control added for admin. Now admin can control whether user can "Start BroadCast" or "Not".

### Added

- d3edc30 , c1d99a3 , 18fa994 - Channel create by user settings and is user as default paid user 

### Added 

- 29cf350 Master user login added 

### Fixed

- 66da49d Social login - cancel button error fixed


2017-04-07 : Email Features Added for user

2017-04-17 : API Youtube link issue fixed - Refer Commit ID -  4697c37


## 30.12.2017

- User side, - Added Subscription History.

- User side, Added PPV History.

## 09.01.2017

- User side - comment & rating issue fixed.

- Admin panel - Revenue Dashboard 

- Strip card - added cardname

- Subscription - Expiry date issue fixed

- Restricted - From Embedded link other users can't see ppv video 

- Video having recurring payment. Once the user seen the page will reload

- Payper view issues fixed