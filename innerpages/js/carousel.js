$(document).ready(function() {

	//	carousel for latest news
	$(".owl-one").owlCarousel({

		autoPlay: 10000, //Set AutoPlay to 3 seconds

        video:true,
		loop: true,
		rewind: true,
		autoplay: false,
		navText: ['<i class="fas fa-chevron-left cara-left"></i>','<i class="fas fa-chevron-right cara-right"></i>'],
        center: true,
        items:4,
		itemsDesktop: [1199, 3],
		itemsDesktopSmall: [979, 2],
		margin: 10,
		dots: true,
		nav: true,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1,
				nav: true
			},
			600: {
				items: 2,
				nav: true
			},
			1000: {
				items: 3,
				nav: true
			}
		}

	});

});




//scroll-animation for navbar

//window.sr = ScrollReveal();
//sr.reveal('.navbar', {
//	duration: 2000,
//	origin: 'top',
//	distance: '100px',
//	opacity: 0.3,
//	delay: 0,
//	easing: 'cubic-bezier(.03, .93, .11, 1.03)',
//});
//
//sr.reveal('.cl_recreating', {
//	duration: 4000,
//	origin: 'bottom',
//	viewFactor: 0.4,
//	delay: 500,
//	opacity: 0,
////	scale: 0.7,
//	easing: 'cubic-bezier(.03, 1.38, .31, 1.03)',
//});
//sr.reveal('.recreating_btns', {
//	duration: 3000,
//	origin: 'bottom',
////	distance: '200px',
//	viewFactor: 0.4,
//	delay: 1000,
//	opacity: 0.1,
//	easing: 'cubic-bezier(0, 1.01, 0, 1.05)',
//});
//
////sr.reveal('.bg2', {
////	duration: 2000,
////	origin: 'bottom',
////	viewFactor: 0.4,
////	distance: '200px',
////	delay: 1000,
////	easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
////});
//
//sr.reveal('.transaction_animation', {
//	duration: 2000,
//	origin: 'top',
//	viewFactor: 0.5,
//	scale: 0.3,
//	distance: '200px',
//	delay: 100,
//	easing: 'cubic-bezier(0, 1.43, .43, .53)',
//});
//sr.reveal('.move_animation1', {
//	duration: 2000,
//	origin: 'top',
//	viewFactor: 0.5,
//	scale: 0.4,
//	distance: '200px',
//	delay: 100,
//	easing: 'cubic-bezier(0, .66, .27, 1.01)',
//});
//sr.reveal('.move_animation2', {
//	duration: 1500,
//	origin: 'left',
//	distance: '200px',
//	scale: 0.1,
//	opacity: 0.0,
//	delay: 0,
////	easing: 'cubic-bezier(.21, 1.44, .38, 1.23)',
//	rotate: { x: 0, y: 10, z: 180 },
//});
//sr.reveal('.move_animation3', {
//	duration: 1500,
//	origin: 'right',
//	distance: '200px',
//	scale: 0.1,
//	opacity: 0.0,
//	delay: 100,
////	easing: 'cubic-bezier(.21, 1.44, .38, 1.23)',
//	rotate: { x: 0, y: 10, z: -180 },
//});
//sr.reveal('.move_animation4', {
//	duration: 1000,
//	origin: 'bottom',
//	viewFactor: 0.4,
//	delay: 500,
//	scale: 0.4,
//	easing: 'cubic-bezier(.35, 1.44, .31, 1.11)',
//});
//sr.reveal('.move_animation5', {
//	duration: 1500,
//	origin: 'bottom',
//	viewFactor: 0.4,
//	delay: 600,
//	scale: 0.8,
//	easing: 'cubic-bezier(.35, 1.44, .31, 1.11)',
//});
//sr.reveal('.move_animation6', {
//	duration: 2000,
//	origin: 'bottom',
//	viewFactor: 0.4,
//	delay: 700,
//	scale: 0.6,
//	easing: 'cubic-bezier(.35, 1.44, .31, 1.11)',
//});
//
//sr.reveal('.cl_work1', {
//	duration: 1000,
//	origin: 'left',
//	viewFactor: 0.5,
//	opacity: 0.0,
//	scale: 0.8,
//	distance: '200px',
//});
//sr.reveal('.dottedsvg', {
//	duration: 1000,
//	origin: 'left',
//	viewFactor: 0.5,
//	delay: 500,
//	opacity: 0.0,
//	scale: 0.8,
//	distance: '200px',
//});
//sr.reveal('.work_text', {
//	duration: 1000,
//	origin: 'right',
//	viewFactor: 0.5,
//	scale: 0.8,
//	opacity: 0.0,
//	distance: '200px',
//});
//
////2
//sr.reveal('.work_text2', {
//	duration: 1000,
//	origin: 'left',
//	viewFactor: 0.55,
//	opacity: 0.0,
//	scale: 0.8,
//	distance: '200px',
//});
//sr.reveal('.dottedsvg2', {
//	duration: 1000,
//	origin: 'right',
//	viewFactor: 0.6,
//	delay: 500,
//	opacity: 0.0,
//	scale: 0.8,
//	distance: '200px',
//	rotate: { x: 180, y: 0, z: 0 },
//});
//sr.reveal('.cl_work2', {
//	duration: 1000,
//	origin: 'right',
//	viewFactor: 0.55,
//	scale: 0.8,
//	opacity: 0.0,
//	distance: '200px',
//});
//
//
//sr.reveal('.cl_work3', {
//	duration: 1000,
//	origin: 'left',
//	viewFactor: 0.59,
//	opacity: 0.0,
//	scale: 0.8,
//	distance: '200px',
//});
//sr.reveal('.dottedsvg3', {
//	duration: 1000,
//	origin: 'left',
//	viewFactor: 0.59,
//	delay: 500,
//	opacity: 0.0,
//	scale: 0.8,
//	distance: '200px',
//});
//sr.reveal('.work_text3', {
//	duration: 1000,
//	origin: 'right',
//	viewFactor: 0.59,
//	scale: 0.8,
//	opacity: 0.0,
//	distance: '200px',
//});
//
//
//sr.reveal('.work_text4', {
//	duration: 1000,
//	origin: 'left',
//	viewFactor: 0.6,
//	opacity: 0.0,
//	scale: 0.8,
//	distance: '200px',
//});
//sr.reveal('.dottedsvg4', {
//	duration: 1000,
//	origin: 'right',
//	viewFactor: 0.6,
//	delay: 500,
//	opacity: 0.0,
//	scale: 0.8,
//	distance: '200px',
//	rotate: { x: 180, y: 0, z: 0 },
//});
//sr.reveal('.cl_work4', {
//	duration: 1000,
//	origin: 'right',
//	viewFactor: 0.65,
//	scale: 0.8,
//	opacity: 0.0,
//	distance: '200px',
//});
//
//sr.reveal('.cl_work5', {
//	duration: 1000,
//	origin: 'left',
//	viewFactor: 0.59,
//	opacity: 0.0,
//	scale: 0.8,
//	distance: '200px',
//});
//sr.reveal('.dottedsvg5', {
//	duration: 1000,
//	origin: 'left',
//	viewFactor: 0.59,
//	delay: 500,
//	opacity: 0.0,
//	scale: 0.8,
//	distance: '200px',
//});
//sr.reveal('.work_text5', {
//	duration: 1000,
//	origin: 'right',
//	viewFactor: 0.59,
//	scale: 0.8,
//	opacity: 0.0,
//	distance: '200px',
//});
