# ************************************************************
# Sequel Pro  dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (My 5.7.17)
# Database: common
# Generation Time: 2018-01-10 19:06:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD__MODE=@@_MODE, _MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD__NOTES=@@_NOTES, _NOTES=0 */;


# Dump of table admins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('male','female','others') COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token_expiry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;

INSERT INTO `admins` (`id`, `unique_id`, `name`, `email`, `password`, `picture`, `gender`, `mobile`, `address`, `description`, `token`, `token_expiry`, `status`, `user_id`, `timezone`, `remember_token`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,'','Admin','admin@tubenow.com','$2y$10$NPgqpfb7GZdmybqL1xX0tOFWACRN4uWoXJlwvO8hKf1JMz1kwc0U2','https://tubenow.streamhash.com/placeholder.png','male','','','','','',0,0,'',NULL,NULL,'2018-01-10 13:35:41','2018-01-10 13:35:41');

/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ads_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ads_details`;

CREATE TABLE `ads_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ad_time` int(11) NOT NULL DEFAULT '0' COMMENT 'In Seconds',
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ad_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table assign_video_ads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assign_video_ads`;

CREATE TABLE `assign_video_ads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_ad_id` int(11) DEFAULT NULL,
  `ad_id` int(11) DEFAULT NULL,
  `ad_type` int(11) NOT NULL DEFAULT '0',
  `ad_time` int(11) NOT NULL DEFAULT '0' COMMENT 'In Sec',
  `video_time` time DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table banner_ads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `banner_ads`;

CREATE TABLE `banner_ads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table cards
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cards`;

CREATE TABLE `cards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `customer_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_four` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `cvv` int(11) NOT NULL,
  `is_default` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table channel_subscriptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `channel_subscriptions`;

CREATE TABLE `channel_subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table channels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `channels`;

CREATE TABLE `channels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_approved` int(11) NOT NULL COMMENT 'Admin can enable or disable',
  `status` int(11) NOT NULL COMMENT 'User can enable and disable the channel',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table chat_messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `chat_messages`;

CREATE TABLE `chat_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `live_video_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `live_video_viewer_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('uv','vu') COLLATE utf8_unicode_ci NOT NULL COMMENT 'uv - User To Viewer , pu - Viewer to User',
  `delivered` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table flags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flags`;

CREATE TABLE `flags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key, It is an unique key',
  `user_id` int(10) unsigned NOT NULL,
  `video_tape_id` int(10) unsigned NOT NULL,
  `reason` longtext COLLATE utf8_unicode_ci COMMENT 'Reason for flagging the video',
  `status` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Status of the flag table',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table languages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `folder_name` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;

INSERT INTO `languages` (`id`, `folder_name`, `language`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'en','English',1,'2018-01-10 13:35:41','2018-01-10 13:35:41');

/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table like_dislike_videos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `like_dislike_videos`;

CREATE TABLE `like_dislike_videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_tape_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `like_status` int(11) NOT NULL,
  `dislike_status` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table live_video_payments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `live_video_payments`;

CREATE TABLE `live_video_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `live_video_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `live_video_viewer_id` int(11) NOT NULL,
  `payment_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_mode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `admin_amount` double(8,2) NOT NULL,
  `user_amount` double(8,2) NOT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table live_videos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `live_videos`;

CREATE TABLE `live_videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `channel_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `virtual_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Public, Private',
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT '0.00',
  `is_streaming` int(11) NOT NULL DEFAULT '0',
  `snapshot` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` text COLLATE utf8_unicode_ci NOT NULL,
  `viewer_cnt` int(11) NOT NULL DEFAULT '0',
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `no_of_minutes` int(11) DEFAULT '0',
  `port_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2014_10_12_000000_create_users_table',1),
	('2014_10_12_100000_create_password_resets_table',1),
	('2015_08_25_172600_create_settings_table',1),
	('2016_07_25_142335_create_admins_table',1),
	('2016_08_02_134552_create_user_ratings_table',1),
	('2016_08_02_143110_create_wishlists_table',1),
	('2016_08_02_144545_create_user_histories_table',1),
	('2016_08_07_122712_create_pages_table',1),
	('2016_08_19_134019_create_user_payments_table',1),
	('2016_08_29_073204_create_mobile_registers_table',1),
	('2016_08_29_082431_create_page_counters_table',1),
	('2016_09_15_070030_create_jobs_table',1),
	('2016_09_15_070051_create_failed_jobs_table',1),
	('2017_01_31_114409_create_user_tracks_table',1),
	('2017_03_22_124504_create_flags_table',1),
	('2017_03_23_093118_create_pay_per_views_table',1),
	('2017_03_29_135241_create_subscriptions_table',1),
	('2017_04_12_085551_create_language_table',1),
	('2017_05_03_071458_create_channels_table',1),
	('2017_05_03_073235_create_video_tapes_table',1),
	('2017_05_12_133310_added_picture_field_in_subscription',1),
	('2017_05_15_065503_create_ads_table',1),
	('2017_05_15_065932_create_ads_details',1),
	('2017_05_15_111308_added_ads_status_in_users_table',1),
	('2017_05_16_051837_added_ratings_field_in_videos_taps',1),
	('2017_05_19_092338_add_video_path_in_video_tapes',1),
	('2017_05_19_112414_create_video_tap_images_table',1),
	('2017_05_23_064843_zero_subscription_status',1),
	('2017_05_23_080552_added_compress_status_in_video_tapes',1),
	('2017_05_23_082939_added_ad_stauts_in_video_tapes',1),
	('2017_05_23_091725_added_title_in_pages',1),
	('2017_05_23_104425_added_amount_field_in_video_tapes',1),
	('2017_05_24_151437_create_redeems_table',1),
	('2017_05_24_161212_create_redeem_requests_table',1),
	('2017_05_27_060308_added_is_banner_field_in_video_tapes',1),
	('2017_05_29_070120_added_redeem_count_in_video_tapes_table',1),
	('2017_05_29_192415_create_assign_ad_table',1),
	('2017_05_31_101056_add_user_id_to_video_tapes_table',1),
	('2017_05_31_152258_add_ad_url_key_in_ad_details',1),
	('2017_07_14_121322_create_add_cards_table',1),
	('2017_08_14_085732_added_subtitle_to_video_tapes',1),
	('2017_08_14_091703_create_channel_subscriptions',1),
	('2017_08_14_092159_create_like_dislike_videos',1),
	('2017_08_25_141223_added_dob_in_users',1),
	('2017_08_26_083313_added_user_ratings_in_video_tapes',1),
	('2017_08_27_091840_added_age_limit_in_users',1),
	('2017_09_01_114848_create_live_video_table',1),
	('2017_09_01_121044_added_commission_split_in_users',1),
	('2017_09_01_121342_create_viewers_table',1),
	('2017_09_01_121541_create_live_video_payments_table',1),
	('2017_09_02_113900_create_chat_messages_table',1),
	('2017_10_12_060457_added_video_type_in_video_tapes',1),
	('2017_10_12_062908_create_banner_ads',1),
	('2017_10_12_075539_added_position_in_banner_ads',1),
	('2017_10_12_122246_added_link_in_banner_ads',1),
	('2017_10_23_131137_add_is_master_user_field_to_users_table',1),
	('2017_10_23_141725_add_user_id_to_admin_table',1),
	('2017_11_15_102653_added_ppv_in_videos_tapes_table',1),
	('2017_11_16_164136_added_amount_fields_in_users',1),
	('2017_12_13_094327_added_fields_in_pay_perviews',1),
	('2017_12_22_182954_add_notes_to_user_payments_table',1),
	('2017_12_22_183016_add_notes_to_pay_per_views_table',1),
	('2017_12_27_074050_add_commission_fields_to_pay_per_views_table',1),
	('2017_12_27_085914_add_commission_spilit_details_to_redeems',1),
	('2018_01_07_061707_added_fields_in_card_details',1),
	('2018_01_07_135716_added_enu_values_pages',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mobile_registers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mobile_registers`;

CREATE TABLE `mobile_registers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `mobile_registers` WRITE;
/*!40000 ALTER TABLE `mobile_registers` DISABLE KEYS */;

INSERT INTO `mobile_registers` (`id`, `type`, `count`, `created_at`, `updated_at`)
VALUES
	(1,'android',0,NULL,NULL),
	(2,'ios',0,NULL,NULL),
	(3,'web',0,NULL,NULL);

/*!40000 ALTER TABLE `mobile_registers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table page_counters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `page_counters`;

CREATE TABLE `page_counters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('about','privacy','terms','help','others','contact') COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table pay_per_views
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pay_per_views`;

CREATE TABLE `pay_per_views` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key, It is an unique key',
  `user_id` int(10) unsigned NOT NULL COMMENT 'User table Primary key given as Foreign Key',
  `video_id` int(10) unsigned NOT NULL COMMENT 'Admin Video table Primary key given as Foreign Key',
  `payment_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `admin_ppv_amount` double(8,2) NOT NULL,
  `user_ppv_amount` double(8,2) NOT NULL,
  `type_of_subscription` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_of_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_date` datetime NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Status of the per_per_view table',
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table redeem_requests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `redeem_requests`;

CREATE TABLE `redeem_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `request_amount` double(8,2) NOT NULL,
  `paid_amount` double(8,2) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table redeems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `redeems`;

CREATE TABLE `redeems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `total` double(8,2) NOT NULL,
  `total_admin_amount` double(8,2) NOT NULL,
  `total_user_amount` double(8,2) NOT NULL,
  `paid` double(8,2) NOT NULL,
  `remaining` double(8,2) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `settings_key_index` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `key`, `value`, `created_at`, `updated_at`)
VALUES
	(1,'site_name','StreamHash',NULL,NULL),
	(2,'site_logo','',NULL,NULL),
	(3,'site_icon','',NULL,NULL),
	(4,'browser_key','',NULL,NULL),
	(5,'default_lang','en',NULL,NULL),
	(6,'currency','$',NULL,NULL),
	(7,'admin_delete_control','0',NULL,NULL),
	(8,'email_verify_control','0',NULL,NULL),
	(9,'is_subscription','0',NULL,NULL),
	(10,'installation_process','0',NULL,NULL),
	(11,'amount','10',NULL,NULL),
	(12,'expiry_days','28',NULL,NULL),
	(13,'admin_take_count','12',NULL,NULL),
	(14,'google_analytics','',NULL,NULL),
	(15,'streaming_url','',NULL,NULL),
	(16,'video_compress_size','50',NULL,NULL),
	(17,'image_compress_size','8',NULL,NULL),
	(18,'track_user_mail','',NULL,NULL),
	(19,'REPORT_VIDEO','Sexual content',NULL,NULL),
	(20,'REPORT_VIDEO','Violent or repulsive content.',NULL,NULL),
	(21,'REPORT_VIDEO','Hateful or abusive content.',NULL,NULL),
	(22,'REPORT_VIDEO','Harmful dangerous acts.',NULL,NULL),
	(23,'REPORT_VIDEO','Child abuse.',NULL,NULL),
	(24,'REPORT_VIDEO','Spam or misleading.',NULL,NULL),
	(25,'REPORT_VIDEO','Infringes my rights.',NULL,NULL),
	(26,'REPORT_VIDEO','Captions issue.',NULL,NULL),
	(27,'VIDEO_RESOLUTIONS','426x240',NULL,NULL),
	(28,'VIDEO_RESOLUTIONS','640x360',NULL,NULL),
	(29,'VIDEO_RESOLUTIONS','854x480',NULL,NULL),
	(30,'VIDEO_RESOLUTIONS','1280x720',NULL,NULL),
	(31,'VIDEO_RESOLUTIONS','1920x1080',NULL,NULL),
	(32,'is_spam','1',NULL,NULL),
	(33,'viewers_count_per_video','10',NULL,NULL),
	(34,'amount_per_video','100',NULL,NULL),
	(35,'minimum_redeem','1',NULL,NULL),
	(36,'redeem_control','1',NULL,NULL),
	(37,'header_scripts','',NULL,NULL),
	(38,'body_scripts','',NULL,NULL),
	(39,'multi_channel_status','0',NULL,NULL),
	(40,'admin_login','','2018-01-10 13:35:41','2018-01-10 13:35:41'),
	(41,'admin_password','','2018-01-10 13:35:41','2018-01-10 13:35:41'),
	(42,'JWPLAYER_KEY','M2NCefPoiiKsaVB8nTttvMBxfb1J3Xl7PDXSaw==','2018-01-10 13:35:41','2018-01-10 13:35:41'),
	(43,'HLS_STREAMING_URL','','2018-01-10 13:35:41','2018-01-10 13:35:41'),
	(44,'post_max_size','2000M',NULL,NULL),
	(45,'upload_max_size','2000M',NULL,NULL),
	(46,'admin_language_control','1',NULL,NULL),
	(47,'stripe_publishable_key','pk_test_uDYrTXzzAuGRwDYtu7dkhaF3',NULL,NULL),
	(48,'stripe_secret_key','sk_test_lRUbYflDyRP3L2UbnsehTUHW',NULL,NULL),
	(49,'age_limit','18',NULL,NULL),
	(50,'SOCKET_URL','','2018-01-10 13:35:41','2018-01-10 13:35:41'),
	(51,'BASE_URL','/','2018-01-10 13:35:41','2018-01-10 13:35:41'),
	(52,'cross_platform_url','',NULL,NULL),
	(53,'mobile_rtsp','',NULL,NULL),
	(54,'wowza_server_url','',NULL,NULL),
	(55,'kurento_socket_url','',NULL,NULL),
	(56,'admin_commission','10',NULL,NULL),
	(57,'user_commission','90',NULL,NULL),
	(58,'max_register_age_limit','15',NULL,NULL),
	(59,'chat_socket_url','',NULL,NULL),
	(60,'chat_url','',NULL,NULL),
	(61,'is_banner_video','0',NULL,NULL),
	(62,'is_banner_ad','0',NULL,NULL),
	(63,'is_vod','0','2018-01-10 13:35:41','2018-01-10 13:35:41'),
	(64,'wowza_ip_address','','2018-01-10 13:35:41','2018-01-10 13:35:41'),
	(65,'create_channel_by_user','1',NULL,NULL),
	(66,'broadcast_by_user','1',NULL,NULL),
	(67,'master_user_login','1',NULL,NULL),
	(68,'delete_video_hour','2',NULL,NULL),
	(69,'admin_ppv_commission','10','2018-01-10 13:35:41','2018-01-10 13:35:41'),
	(70,'user_ppv_commission','90','2018-01-10 13:35:41','2018-01-10 13:35:41'),
	(71,'is_payper_view','1',NULL,NULL),
	(72,'facebook_link','',NULL,NULL),
	(73,'linkedin_link','',NULL,NULL),
	(74,'twitter_link','',NULL,NULL),
	(75,'google_plus_link','',NULL,NULL),
	(76,'pinterest_link','',NULL,NULL),
	(77,'appstore','',NULL,NULL),
	(78,'playstore','',NULL,NULL),
	(79,'push_notification','1',NULL,NULL);

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subscriptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subscriptions`;

CREATE TABLE `subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `subscription_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'month,year,days',
  `plan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double(8,2) NOT NULL,
  `total_subscription` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_histories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_histories`;

CREATE TABLE `user_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `video_tape_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_payments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_payments`;

CREATE TABLE `user_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL,
  `payment_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `expiry_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_ratings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_ratings`;

CREATE TABLE `user_ratings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `video_tape_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_tracks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_tracks`;

CREATE TABLE `user_tracks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` text COLLATE utf8_unicode_ci NOT NULL,
  `HTTP_USER_AGENT` text COLLATE utf8_unicode_ci NOT NULL,
  `REQUEST_TIME` text COLLATE utf8_unicode_ci NOT NULL,
  `REMOTE_ADDR` text COLLATE utf8_unicode_ci NOT NULL,
  `hostname` text COLLATE utf8_unicode_ci NOT NULL,
  `latitude` double(10,8) NOT NULL,
  `longitude` double(10,8) NOT NULL,
  `origin` text COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `others` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` text COLLATE utf8_unicode_ci NOT NULL,
  `token_expiry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL,
  `paypal_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chat_picture` text COLLATE utf8_unicode_ci,
  `dob` date NOT NULL,
  `age_limit` int(11) NOT NULL DEFAULT '0',
  `device_type` enum('web','android','ios') COLLATE utf8_unicode_ci NOT NULL,
  `device_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `register_type` enum('web','android','ios') COLLATE utf8_unicode_ci NOT NULL,
  `login_by` enum('manual','facebook','twitter','google','linkedin') COLLATE utf8_unicode_ci NOT NULL,
  `social_unique_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('male','female','others') COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` double(15,8) NOT NULL,
  `longitude` double(15,8) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_mode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_id` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1 - Approve , 0 - decline',
  `is_master_user` int(11) NOT NULL,
  `total_amount` double(8,2) NOT NULL,
  `total_admin_amount` double(8,2) NOT NULL,
  `total_user_amount` double(8,2) NOT NULL,
  `paid_amount` double(8,2) NOT NULL,
  `remaining_amount` double(8,2) NOT NULL,
  `role` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zero_subscription_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `push_status` int(11) NOT NULL COMMENT 'Mobile Purpose',
  `verification_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `verification_code_expiry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_verified` int(11) NOT NULL DEFAULT '0' COMMENT '1 - verified , 0 - No',
  `is_moderator` int(11) NOT NULL,
  `moderator_id` int(11) NOT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ads_status` int(11) NOT NULL DEFAULT '0' COMMENT ' 0 - Disabled, 1 - Enabled',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `amount_paid` double(8,2) NOT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `no_of_days` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `unique_id`, `name`, `email`, `password`, `token`, `token_expiry`, `user_type`, `paypal_email`, `picture`, `chat_picture`, `dob`, `age_limit`, `device_type`, `device_token`, `register_type`, `login_by`, `social_unique_id`, `description`, `gender`, `mobile`, `latitude`, `longitude`, `address`, `payment_mode`, `card_id`, `status`, `is_master_user`, `total_amount`, `total_admin_amount`, `total_user_amount`, `paid_amount`, `remaining_amount`, `role`, `zero_subscription_status`, `push_status`, `verification_code`, `verification_code_expiry`, `is_verified`, `is_moderator`, `moderator_id`, `timezone`, `ads_status`, `remember_token`, `deleted_at`, `created_at`, `updated_at`, `amount_paid`, `expiry_date`, `no_of_days`)
VALUES
	(1,'','User','user@tubenow.com','$2y$10$ulVUazFuN9t.zESSWTwJP.5W0A1J0q6gYHV7CSEbCKLyzhb.UwArS','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1MTU2MTExNDEsImp0aSI6NDExOTc3MTcxLCJpc3MiOiJsb2NhbGhvc3QiLCJuYmYiOjE1MTU2MTExNDEsImV4cCI6OTA5MzY2Njg0NjAwMCwiZGF0YSI6eyJpZCI6MSwiZW1haWwiOiJ1c2VyQHR1YmVub3cuY29tIiwicm9sZSI6Im1vZGVsIn19.X2xhF0Wb-55nFdoTL2UHQTjwdZEpgrA42IiBJFQtrMPlgazb_foopw6xXqLsBN33ebjZSMgYyPSW9n726RItzg','1518203141',0,'','https://tubenow.streamhash.com/placeholder.png','https://tubenow.streamhash.com/placeholder.png','1992-01-01',25,'web','','web','manual','','','male','',0.00000000,0.00000000,'','',0,1,0,0.00,0.00,0.00,0.00,0.00,NULL,'0',0,'','',1,0,0,'',0,NULL,NULL,'2018-01-10 13:35:41','2018-01-10 13:35:41',0.00,NULL,0);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table video_ads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `video_ads`;

CREATE TABLE `video_ads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_tape_id` int(11) NOT NULL,
  `types_of_ad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1 - Pre Ad, 2 - Post Ad, 3 - In Between Ad',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table video_tape_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `video_tape_images`;

CREATE TABLE `video_tape_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_tape_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table video_tapes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `video_tapes`;

CREATE TABLE `video_tapes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `unique_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `default_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age_limit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` time DEFAULT NULL,
  `video_publish_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '1 - publish now , 2 Publish later',
  `publish_time` datetime NOT NULL,
  `is_approved` int(11) NOT NULL COMMENT 'Admin Approve and UnApprove',
  `status` int(11) NOT NULL COMMENT 'User Approve and UnApprove',
  `video_type` int(11) NOT NULL,
  `is_banner` int(11) NOT NULL DEFAULT '0',
  `banner_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double(8,2) NOT NULL DEFAULT '0.00',
  `watch_count` int(11) NOT NULL,
  `redeem_count` int(11) NOT NULL DEFAULT '0',
  `reviews` text COLLATE utf8_unicode_ci,
  `video_path` text COLLATE utf8_unicode_ci,
  `video_resolutions` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publish_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `compress_status` int(11) NOT NULL DEFAULT '0',
  `ad_status` int(11) NOT NULL DEFAULT '0',
  `ratings` int(11) NOT NULL DEFAULT '0',
  `user_ratings` int(11) NOT NULL,
  `type_of_user` int(11) NOT NULL DEFAULT '0',
  `type_of_subscription` int(11) NOT NULL DEFAULT '0',
  `ppv_amount` double(8,2) NOT NULL DEFAULT '0.00',
  `admin_ppv_amount` double(8,2) NOT NULL DEFAULT '0.00',
  `user_ppv_amount` double(8,2) NOT NULL DEFAULT '0.00',
  `ppv_created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table viewers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `viewers`;

CREATE TABLE `viewers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table wishlists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wishlists`;

CREATE TABLE `wishlists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `video_tape_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET _NOTES=@OLD__NOTES */;
/*!40101 SET _MODE=@OLD__MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
