<?php


use Illuminate\Support\Facades\Redis;


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Report Video type


if(!defined('PUSH_TO_ALL')) define('PUSH_TO_ALL', 0);

if(!defined('PUSH_TO_CHANNEL_SUBSCRIBERS')) define('PUSH_TO_CHANNEL_SUBSCRIBERS', 1);

if(!defined('PUSH_REDIRECT_HOME')) define('PUSH_REDIRECT_HOME', 1);
if(!defined('PUSH_REDIRECT_CHANNEL')) define('PUSH_REDIRECT_CHANNEL', 2);
if(!defined('PUSH_REDIRECT_SINGLE_VIDEO')) define('PUSH_REDIRECT_SINGLE_VIDEO', 3);
if(!defined('PAID_STATUS')) define('PAID_STATUS', 1);
if(!defined('AUTORENEWAL_ENABLED')) define('AUTORENEWAL_ENABLED',0);

if(!defined('AUTORENEWAL_CANCELLED')) define('AUTORENEWAL_CANCELLED',1);
if(!defined('DEVICE_ANDROID')) define('DEVICE_ANDROID', 'android');

if(!defined('DEVICE_IOS')) define('DEVICE_IOS', 'ios');

if(!defined('DEVICE_WEB')) define('DEVICE_WEB', 'web');

// if (!defined('RTMP_URL')) define('RTMP_URL', 'rtmp://'.Setting::get('cross_platform_url').'/live/');

// Channel settings 

if(!defined('CREATE_CHANNEL_BY_USER_ENABLED')) define('CREATE_CHANNEL_BY_USER_ENABLED' , 1);

if(!defined('CREATE_CHANNEL_BY_USER_DISENABLED')) define('CREATE_CHANNEL_BY_USER_DISENABLED' , 0);

// REDEEMS

if(!defined('REDEEM_OPTION_ENABLED')) define('REDEEM_OPTION_ENABLED', 1);

if(!defined('REDEEM_OPTION_DISABLED')) define('REDEEM_OPTION_DISABLED', 0);

// Redeeem Request Status

if(!defined('REDEEM_REQUEST_SENT')) define('REDEEM_REQUEST_SENT', 0);
if(!defined('REDEEM_REQUEST_PROCESSING')) define('REDEEM_REQUEST_PROCESSING', 1);
if(!defined('REDEEM_REQUEST_PAID')) define('REDEEM_REQUEST_PAID', 2);
if(!defined('REDEEM_REQUEST_CANCEL')) define('REDEEM_REQUEST_CANCEL', 3);

if(!defined('TYPE_PUBLIC')) define('TYPE_PUBLIC', 'public');
if(!defined('TYPE_PRIVATE')) define('TYPE_PRIVATE', 'private');

// Ad Types

if(!defined('PRE_AD')) define('PRE_AD', 1);
if(!defined('POST_AD')) define('POST_AD', 2);
if(!defined('BETWEEN_AD')) define('BETWEEN_AD', 3);

if(!defined('REPORT_VIDEO_KEY')) define('REPORT_VIDEO_KEY', 'REPORT_VIDEO');
if (!defined('IMAGE_RESOLUTIONS_KEY')) define('IMAGE_RESOLUTIONS_KEY', 'IMAGE_RESOLUTIONS');
if (!defined('VIDEO_RESOLUTIONS_KEY')) define('VIDEO_RESOLUTIONS_KEY', 'VIDEO_RESOLUTIONS');

// User Type
if(!defined('NORMAL_USER')) define('NORMAL_USER', 1);
if(!defined('PAID_USER')) define('PAID_USER', 2);
if(!defined('BOTH_USERS')) define('BOTH_USERS', 3);

// Subscription Type
if(!defined('ONE_TIME_PAYMENT')) define('ONE_TIME_PAYMENT', 1);
if(!defined('RECURRING_PAYMENT')) define('RECURRING_PAYMENT', 2);

// REQUEST STATE

if(!defined('REQUEST_STEP_1')) define('REQUEST_STEP_1', 1);
if(!defined('REQUEST_STEP_2')) define('REQUEST_STEP_2', 2);
if(!defined('REQUEST_STEP_3')) define('REQUEST_STEP_3', 3);
if(!defined('REQUEST_STEP_FINAL')) define('REQUEST_STEP_FINAL', 4);


if(!defined('USER')) define('USER', 0);

if(!defined('Moderator')) define('Moderator',1);

if(!defined('NONE')) define('NONE', 0);

if(!defined('MAIN_VIDEO')) define('MAIN_VIDEO', 1);
if(!defined('TRAILER_VIDEO')) define('TRAILER_VIDEO', 2);


if(!defined('DEFAULT_TRUE')) define('DEFAULT_TRUE', 1);
if(!defined('DEFAULT_FALSE')) define('DEFAULT_FALSE', 0);

if(!defined('ADMIN')) define('ADMIN', 'admin');
if(!defined('MODERATOR')) define('MODERATOR', 'moderator');

if(!defined('VIDEO_TYPE_UPLOAD')) define('VIDEO_TYPE_UPLOAD', 1);
if(!defined('VIDEO_TYPE_LIVE')) define('VIDEO_TYPE_LIVE', 2);


if(!defined('VIDEO_UPLOAD_TYPE_s3')) define('VIDEO_UPLOAD_TYPE_s3', 1);
if(!defined('VIDEO_UPLOAD_TYPE_DIRECT')) define('VIDEO_UPLOAD_TYPE_DIRECT', 2);

if(!defined('NO_INSTALL')) define('NO_INSTALL' , 0);

if(!defined('SYSTEM_CHECK')) define('SYSTEM_CHECK' , 1);

if(!defined('INSTALL_COMPLETE')) define('INSTALL_COMPLETE' , 2);


if(!defined('ADMIN')) define('ADMIN', 'admin');
if(!defined('MODERATOR')) define('MODERATOR', 'moderator');

// Payment Constants
if(!defined('COD')) define('COD',   'cod');
if(!defined('PAYPAL')) define('PAYPAL', 'paypal');
if(!defined('CARD')) define('CARD',  'card');


if(!defined('RATINGS')) define('RATINGS', '0,1,2,3,4,5');


if(!defined('PUBLISH_NOW')) define('PUBLISH_NOW', 1);
if(!defined('PUBLISH_LATER')) define('PUBLISH_LATER', 2);

if(!defined('DEVICE_ANDROID')) define('DEVICE_ANDROID', 'android');
if(!defined('DEVICE_IOS')) define('DEVICE_IOS', 'ios');

if(!defined('WISHLIST_EMPTY')) define('WISHLIST_EMPTY' , 0);
if(!defined('WISHLIST_ADDED')) define('WISHLIST_ADDED' , 1);
if(!defined('WISHLIST_REMOVED')) define('WISHLIST_REMOVED' , 2);

if(!defined('RECENTLY_ADDED')) define('RECENTLY_ADDED' , 'recent');
if(!defined('TRENDING')) define('TRENDING' , 'trending');
if(!defined('SUGGESTIONS')) define('SUGGESTIONS' , 'suggestion');
if(!defined('WISHLIST')) define('WISHLIST' , 'wishlist');
if(!defined('WATCHLIST')) define('WATCHLIST' , 'watchlist');
if(!defined('BANNER')) define('BANNER' , 'banner');
if(!defined('ALL_VIDEOS')) define('ALL_VIDEOS', 'All Videos');
if(!defined('JWT_SECRET')) define('JWT_SECRET', '12345');




if(!defined('WEB')) define('WEB' , 1);


Route::get('/clear-cache', function() {

    $exitCode = Artisan::call('config:cache');
    
    return back();

})->name('clear-cache');

Route::get('/generate/index' , 'ApplicationController@generate_index');


Route::get('/welcome_email' , 'ApplicationController@welcome_email');

Route::get('/forgot_email' , 'ApplicationController@forgot_email');

Route::get('/notification_email' , 'ApplicationController@notification_email');


Route::get('/payment/failure' , 'ApplicationController@payment_failure')->name('payment.failure');

Route::get('/message/save' , 'ApplicationController@message_save');

// Route::get('/subscriptions' , 'ApplicationController@subscriptions')->name('subscriptions.index');

// Route::get('/subscriptions/view' , 'ApplicationController@subscription_view')->name('subscriptions.view');

// Route::get('/videos/create' , 'ApplicationController@video_create')->name('videos.create');


Route::get('/test' , 'ApplicationController@test')->name('test');

Route::post('/test' , 'ApplicationController@test')->name('test');

Route::get('/email/verification' , 'ApplicationController@email_verify')->name('email.verify');

// Installation

Route::get('/install/configure', 'InstallationController@install')->name('installTheme');

Route::get('/system/check', 'InstallationController@system_check_process')->name('system-check');

Route::post('/install/theme', 'InstallationController@theme_check_process')->name('install.theme');

Route::post('/install/settings', 'InstallationController@settings_process')->name('install.settings');

// Elastic Search Test

Route::get('/addIndex', 'ApplicationController@addIndex')->name('addIndex');

Route::get('/addAll', 'ApplicationController@addAllVideoToEs')->name('addAll');


Route::get('/user_session_language/{lang}', 'ApplicationController@set_session_language')->name('user_session_language');

// CRON

Route::get('/publish/video', 'ApplicationController@cron_publish_video')->name('publish');

Route::get('/notification/payment', 'ApplicationController@send_notification_user_payment')->name('notification.user.payment');

Route::get('/payment/expiry', 'ApplicationController@user_payment_expiry')->name('user.payment.expiry');

// Static Pages

Route::get('/privacy', 'UserApiController@privacy')->name('user.privacy');

Route::get('/help', 'UserApiController@help')->name('user.help');

Route::get('/terms_condition', 'UserApiController@terms')->name('user.terms');

Route::get('/privacy_policy', 'ApplicationController@privacy')->name('user.privacy_policy');

Route::get('/terms', 'ApplicationController@terms')->name('user.terms-condition');

Route::get('/about', 'ApplicationController@about')->name('user.about');

// Video upload 

Route::post('select/sub_category' , 'ApplicationController@select_sub_category')->name('select.sub_category');

Route::post('select/genre' , 'ApplicationController@select_genre')->name('select.genre');

Route::get('admin/control', 'ApplicationController@admin_control')->name('control');

Route::post('admin/control', 'ApplicationController@save_admin_control')->name('admin.save.control');

Route::get('page_view/{id}', 'UserController@page_view')->name('page_view');


Route::group(['prefix' => 'admin' , 'as' => 'admin.'], function(){

    Route::get('login', 'Auth\AdminAuthController@showLoginForm')->name('login');

    Route::post('login', 'Auth\AdminAuthController@login')->name('login.post');

    Route::get('logout', 'Auth\AdminAuthController@logout')->name('logout');

    // Registration Routes...

    Route::get('register', 'Auth\AdminAuthController@showRegistrationForm');

    Route::post('register', 'Auth\AdminAuthController@register');

    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'Auth\AdminPasswordController@showResetForm');

    Route::post('password/email', 'Auth\AdminPasswordController@sendResetLinkEmail');

    Route::post('password/reset', 'Auth\AdminPasswordController@reset');

    Route::get('/', 'AdminController@dashboard')->name('dashboard');

    Route::get('/delete_user_ratings', 'AdminController@delete_user_ratings')->name('delete_user_ratings');

    Route::get('/profile', 'AdminController@profile')->name('profile');

    Route::post('/profile/save', 'AdminController@profile_process')->name('save.profile');

    Route::post('/change/password', 'AdminController@change_password')->name('change.password');

    Route::get('/unspam-video/{id}', 'AdminController@unspam_video')->name('unspam-video');

    // users

    Route::get('/user/channels/{id}', 'AdminController@user_channels')->name('users.channels');

    Route::get('/users', 'AdminController@users')->name('users');

    Route::get('/add/user', 'AdminController@add_user')->name('add.user');

    Route::get('/edit/user', 'AdminController@edit_user')->name('edit.user');

    Route::post('/add/user', 'AdminController@add_user_process')->name('save.user');

    Route::get('/delete/user', 'AdminController@delete_user')->name('delete.user');

    Route::get('/view/user/{id}', 'AdminController@view_user')->name('view.user');

    Route::get('/user/upgrade/{id}', 'AdminController@user_upgrade')->name('user.upgrade');

    Route::any('/upgrade/disable', 'AdminController@user_upgrade_disable')->name('user.upgrade.disable');

    Route::get('/redeems/{id?}', 'AdminController@user_redeem_requests')->name('users.redeems');

    Route::get('/user/verify/{id?}', 'AdminController@user_verify_status')->name('users.verify');

    Route::post('/redeems/pay', 'AdminController@user_redeem_pay')->name('users.redeem.pay');

    // User History - admin

    Route::get('/user/history/{id}', 'AdminController@view_history')->name('user.history');

    Route::get('/delete/history/{id}', 'AdminController@delete_history')->name('delete.history');
    
    // User Wishlist - admin

    Route::get('/user/wishlist/{id}', 'AdminController@view_wishlist')->name('user.wishlist');

    Route::get('/delete/wishlist/{id}', 'AdminController@delete_wishlist')->name('delete.wishlist');

    // Spam Videos
    Route::get('/spam-videos', 'AdminController@spam_videos')->name('spam-videos');

    Route::get('/view-users/{id}', 'AdminController@view_users')->name('view-users');
    
    // Categories

    Route::get('/channels', 'AdminController@channels')->name('channels');

    Route::get('/add/channel', 'AdminController@add_channel')->name('add.channel');

    Route::get('/edit/channel/{id}', 'AdminController@edit_channel')->name('edit.channel');

    Route::post('/add/channel', 'AdminController@add_channel_process')->name('save.channel');

    Route::get('/delete/channel', 'AdminController@delete_channel')->name('delete.channel');

    Route::get('/view/channel/{id}', 'AdminController@view_channel')->name('view.channel');

    Route::get('/channel/approve', 'AdminController@approve_channel')->name('channel.approve');

    Route::get('/channel/videos/{id?}', 'AdminController@channel_videos')->name('channel.videos');

    

    // Videos

    Route::get('/videos', 'AdminController@videos')->name('videos');

    Route::get('/reviews', 'AdminController@user_ratings')->name('reviews');

    Route::get('/ad_videos', 'AdminController@ad_videos')->name('ad_videos');

    Route::get('/add/video', 'AdminController@add_video')->name('add.video');

    Route::get('/edit/video/{id}', 'AdminController@edit_video')->name('edit.video');

    Route::post('/edit/video/process', 'AdminController@edit_video_process')->name('save.edit.video');

    Route::get('/view/video', 'AdminController@view_video')->name('view.video');

    Route::post('save_video', 'AdminController@video_save')->name('video_save');

    Route::post('save_default_img', 'AdminController@save_default_img')->name('save_default_img');

    Route::post('upload_video_image', 'AdminController@upload_video_image')->name('upload_video_image');

    Route::post('/save_video_payment/{id}', 'AdminController@save_video_payment')->name('save.video-payment');

    Route::get('/delete/video/{id}', 'AdminController@delete_video')->name('delete.video');

    Route::get('/video/approve/{id}', 'AdminController@approve_video')->name('video.approve');

    Route::get('/video/publish-video/{id}', 'AdminController@publish_video')->name('video.publish-video');

    Route::get('/video/decline/{id}', 'AdminController@decline_video')->name('video.decline');

    Route::get('get_images/{id}', 'AdminController@get_images')->name('get_images');

    // Slider Videos

    Route::get('/slider/video/{id}', 'AdminController@slider_video')->name('slider.video');

    // Banner Videos

    Route::get('/banner/videos', 'AdminController@banner_videos')->name('banner.videos');

    Route::get('/add/banner/video', 'AdminController@add_banner_video')->name('add.banner.video');

    Route::get('/change/banner/video/{id}', 'AdminController@change_banner_video')->name('change.video');
    
    // Payment details

    Route::get('revenues' , 'AdminController@revenues')->name('revenues');
    
    Route::get('ppv_payments' , 'AdminController@ppv_payments')->name('ppv_payments');

    Route::get('/subscription/payments/{id?}' , 'AdminController@subscription_payments')->name('subscription.payments');

    Route::get('/remove_payper_view/{id}', 'AdminController@remove_payper_view')->name('remove_pay_per_view');

    // Settings

    Route::get('settings' , 'AdminController@settings')->name('settings');

    Route::post('save_common_settings' , 'AdminController@save_common_settings')->name('save.common-settings');
    
    Route::post('settings' , 'AdminController@settings_process')->name('save.settings');

    Route::post('settings/email' , 'AdminController@email_settings_process')->name('email.settings.save');

    Route::get('help' , 'AdminController@help')->name('help');

    // Pages

    Route::get('/pages', 'AdminController@pages')->name('pages.index');

    Route::get('/pages/edit/{id}', 'AdminController@page_edit')->name('pages.edit');

    Route::get('/pages/create', 'AdminController@page_create')->name('pages.create');

    Route::post('/pages/create', 'AdminController@page_save')->name('pages.save');

    Route::get('/pages/delete/{id}', 'AdminController@page_delete')->name('pages.delete');

    Route::get('video/payments', 'AdminController@video_payments')->name('videos.payments');


    // Custom Push

    Route::get('/custom/push', 'AdminController@custom_push')->name('push');

    Route::post('/custom/push', 'AdminController@custom_push_process')->name('send.push');


    // Ads

    Route::get('ad_create','AdminController@ad_create')->name('ad_create');

    Route::get('ad_edit','AdminController@ad_edit')->name('ad_edit');

    Route::post('save_ad','AdminController@save_ad')->name('save_ad');

    Route::get('ad_index','AdminController@ad_index')->name('ad_index');

    Route::get('ad_status','AdminController@ad_status')->name('ad_status');

    Route::get('ad_delete','AdminController@ad_delete')->name('ad_delete');

    Route::get('ad_view','AdminController@ad_view')->name('ad_view');

    Route::get('assign_ad', 'AdminController@assign_ad')->name('assign_ad');

    Route::post('assign_ad', 'AdminController@save_assign_ad')->name('assign_ads');


    // Banner Ads

    Route::get('create_banner','AdminController@banner_ads_create')->name('banner-ads.create');

    Route::get('edit_banner','AdminController@banner_ads_edit')->name('banner-ads.edit');

    Route::post('save_banner','AdminController@banner_ads_save')->name('banner-ads.save-banner-ad');

    Route::get('banner_ads','AdminController@banner_ads')->name('banner-ads.index');

    Route::get('banner_ad_status/{id}','AdminController@banner_ads_status')->name('banner-ads.status');

    Route::get('delete_banner','AdminController@banner_ads_delete')->name('banner-ads.delete');

    Route::get('view_banner_ad','AdminController@banner_ads_view')->name('banner-ads.view');

    Route::post('banner-position','AdminController@banner_ads_position')->name('banner-ads.position');


    Route::get('ads_create/{video_tape_id}','AdminController@ads_create')->name('ads_create');

    Route::post('save_ads','AdminController@save_ads')->name('save_ads');

    Route::get('ads_edit/{id}','AdminController@ads_edit')->name('ads_edit');

    Route::get('ads_delete','AdminController@ads_delete')->name('ads_delete');

    // Route::get('ads_index','AdminController@ads_index')->name('ads_index');

    Route::get('ads_view','AdminController@ads_view')->name('ads_view');

    Route::post('add_between_ads', 'AdminController@add_between_ads')->name('add.between_ads');


    // Subscriptions


    Route::get('/user_subscriptions/{id}', 'AdminController@user_subscriptions')->name('subscriptions.plans');

    Route::get('/subscription/save/{s_id}/u_id/{u_id}', 'AdminController@user_subscription_save')->name('subscription.save');


    Route::get('/subscriptions', 'AdminController@subscriptions')->name('subscriptions.index');

    Route::get('/subscriptions/create', 'AdminController@subscription_create')->name('subscriptions.create');

    Route::get('/subscriptions/edit/{id}', 'AdminController@subscription_edit')->name('subscriptions.edit');

    Route::post('/subscriptions/create', 'AdminController@subscription_save')->name('subscriptions.save');

    Route::get('/subscriptions/delete/{id}', 'AdminController@subscription_delete')->name('subscriptions.delete');

    Route::get('/subscriptions/view/{id}', 'AdminController@subscription_view')->name('subscriptions.view');

    Route::get('/subscriptions/status/{id}', 'AdminController@subscription_status')->name('subscriptions.status');

    Route::get('/subscribers', 'AdminController@subscribers')->name('subscribers');

    Route::get('/unsubscribe_channel', 'UserController@unsubscribe_channel')->name('channels.unsubscribe');

    Route::post('/users/payout', 'AdminController@user_payout')->name('users.payout');


    // Videos

    Route::get('/live_videos', 'AdminController@live_videos')->name('videos.index');

    Route::get('/videos_list', 'AdminController@videos_list')->name('videos.videos_list');

    Route::get('/videos/view/{id}', 'AdminController@videos_view')->name('videos.view');


     // Languages
    Route::get('/languages/index', 'LanguageController@languages_index')->name('languages.index'); 

    Route::get('/languages/download', 'LanguageController@languages_download')->name('languages.download'); 

    Route::get('/languages/create', 'LanguageController@languages_create')->name('languages.create');
    
    Route::get('/languages/edit/{id}', 'LanguageController@languages_edit')->name('languages.edit');

    Route::get('/languages/status/{id}', 'LanguageController@languages_status')->name('languages.status');   

    Route::post('/languages/save', 'LanguageController@languages_save')->name('languages.save');

    Route::get('/languages/delete/{id}', 'LanguageController@languages_delete')->name('languages.delete');

    Route::get('/languages/set_default_language/{name}', 'LanguageController@set_default_language')->name('languages.set_default_language');


});


Route::get('/user/searchall' , 'ApplicationController@search_video')->name('search');

Route::any('/user/search' , 'ApplicationController@search_all')->name('search-all');


// Social Login

Route::post('/social', array('as' => 'SocialLogin' , 'uses' => 'SocialAuthController@redirect'));

Route::get('/callback/{provider}', 'SocialAuthController@callback');

// Embed Links

Route::get('/embed', 'ApplicationController@embed_video')->name('embed_video');

// Admin to users login

Route::get('/master/login', 'UserController@master_login')->name('master.login');



Route::group(['as' => 'user.'], function(){
    Route::get('/', 'UserController@homePage')->name('home1');

    Route::get('/dashboard', 'UserController@index')->name('dashboard');
  

    Route::get('/trending', 'UserController@trending')->name('trending');

    Route::get('/videos_view/{id}', 'UserController@videos_view')->name('videos_view');

    Route::get('channel_list', 'UserController@channel_list')->name('channel.list');

    Route::get('history', 'UserController@history')->name('history');

    Route::get('wishlist', 'UserController@wishlist')->name('wishlist');

    Route::get('channel/{id}', 'UserController@channel_videos')->name('channel');

    Route::get('video/{id}', 'UserController@single_video')->name('single');

    // Wishlist

    Route::post('addWishlist', 'UserController@add_wishlist')->name('add.wishlist');

    Route::get('deleteWishlist', 'UserController@delete_wishlist')->name('delete.wishlist');


    // Comments

    Route::post('addComment', 'UserController@add_comment')->name('add.comment');


    Route::get('deleteHistory', 'UserController@delete_history')->name('delete.history');

    Route::post('addHistory', 'UserController@add_history')->name('add.history');


    Route::get('delete-video/{id}/{user_id}', 'UserController@delete_video')->name('delete_video');

    Route::get('ppv-video','PaypalController@videoSubscriptionPay')->name('ppv-video-payment');

    Route::get('user/payment/video-status','PaypalController@getVideoPaymentStatus')->name('paypalstatus');


    Route::get('paypal_video','PaypalController@payPerVideo')->name('live_video_paypal');

    Route::get('user/payment/livevideo-status','PaypalController@getLiveVideoPaymentStatus')->name('live-paypalstatus');


    Route::get('login', 'Auth\AuthController@showLoginForm')->name('login.form');

    Route::post('login', 'Auth\AuthController@login')->name('login.post');

    Route::get('logout', 'Auth\AuthController@logout')->name('logout');

    // Registration Routes...
    Route::get('register', 'Auth\AuthController@showRegistrationForm')->name('register.form');

    Route::post('register', 'Auth\AuthController@register')->name('register.post');

    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');

    Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');

    Route::post('password/reset', 'Auth\PasswordController@reset');

    // Subscribe

    Route::get('/subscribe_channel', 'UserController@subscribe_channel')->name('subscribe.channel');

    Route::get('/unsubscribe_channel', 'UserController@unsubscribe_channel')->name('unsubscribe.channel');

    Route::get('/subscribers', 'UserController@channel_subscribers')->name('channel.subscribers');

    Route::post('take_snapshot/{rid}', 'UserController@setCaptureImage')->name('setCaptureImage');
    
    Route::post('/subscriptions/enable', 'UserController@subscriptions_autorenewal_enable')->name('subscriptions.enable-subscription');

    Route::post('/subscriptions/pause', 'UserController@subscriptions_autorenewal_pause')->name('subscriptions.pause-subscription');

    Route::get('profile', 'UserController@profile')->name('profile');

    Route::get('update/profile', 'UserController@update_profile')->name('update.profile');

    Route::post('update/profile', 'UserController@profile_save')->name('profile.save');

    Route::get('/profile/password', 'UserController@profile_change_password')->name('change.password');

    Route::post('/profile/password', 'UserController@profile_save_password')->name('profile.password');

    // Delete Account

    Route::get('/delete/account', 'UserController@delete_account')->name('delete.account');

    Route::post('/delete/account', 'UserController@delete_account_process')->name('delete.account.process');


    // Channels

    Route::get('channel_create', 'UserController@channel_create')->name('create_channel');

    Route::post('save_channel', 'UserController@save_channel')->name('save_channel');

    Route::get('channel_edit/{id}', 'UserController@channel_edit')->name('channel_edit');

    Route::get('delete_channel', 'UserController@channel_delete')->name('delete.channel');



    // Report Spam Video

    Route::post('markSpamVideo', 'UserController@save_report_video')->name('add.spam_video');

    Route::get('unMarkSpamVideo/{id}', 'UserController@remove_report_video')->name('remove.report_video');

    Route::get('spamVideos', 'UserController@spam_videos')->name('spam-videos');

    Route::get('payment-video', 'UserController@payment_url')->name('payment_url');

    Route::get('stripe-payment-video', 'UserController@stripe_payment_video')->name('stripe_payment_video');
    

    Route::post('/save_video_payment/{id}', 'UserController@save_video_payment')->name('save.video-payment');

    Route::get('/remove_payper_view/{id}', 'UserController@remove_payper_view')->name('remove_pay_per_view');


    // Paypal Payment
    Route::get('/paypal/{id}','PaypalController@pay')->name('paypal');

    Route::get('/user/payment/status','PaypalController@getPaymentStatus')->name('paypalstatus');

    Route::get('/live_videos', 'UserController@live_videos')->name('live_videos');

    Route::get('/subscriptions', 'UserController@subscriptions')->name('subscriptions');

    Route::get('/subscription/save/{s_id}/u_id/{u_id}', 'UserController@user_subscription_save')->name('subscription.save');
    

    // Video Upload

    Route::get('upload_video', 'UserController@video_upload')->name('video_upload');

    Route::post('video_save', 'UserController@video_save')->name('video_save');

    Route::post('save_default_img', 'UserController@save_default_img')->name('save_default_img');

    Route::post('upload_video_image', 'UserController@upload_video_image')->name('upload_video_image');

    Route::post('ad_request', 'UserController@ad_request')->name('ad_request');

    Route::get('/delete/video/{id}', 'UserController@video_delete')->name('delete.video');

    Route::get('/edit_video/{id}', 'UserController@video_edit')->name('edit.video');

    Route::get('get_images/{id}', 'UserController@get_images')->name('get_images');

    Route::post('/like_video', 'UserController@likeVideo')->name('video.like');

    Route::post('/dis_like_video', 'UserController@disLikeVideo')->name('video.disLike');

    // Redeems

    Route::get('redeems/', 'UserController@redeems')->name('redeems');

    Route::get('send/redeem', 'UserController@send_redeem_request')->name('redeems.send.request');

    Route::get('redeem/request/cancel/{id?}', 'UserController@redeem_request_cancel')->name('redeems.request.cancel');


    Route::get('/card', 'UserController@card_details')->name('card.card_details');

    Route::post('/add-card', 'UserController@payment_card_add')->name('card.add_card');


    Route::post('/pay_tour','UserController@pay_tour')->name('card.card');

    Route::patch('/payment', 'UserController@payment_card_default')->name('card.default');

    Route::delete('/payment', 'UserController@payment_card_delete')->name('card.delete');

    Route::get('/stripe_payment', 'UserController@stripe_payment')->name('card.stripe_payment');

    Route::get('/ppv-stripe-payment', 'UserController@ppv_stripe_payment')->name('card.ppv-stripe-payment');

    Route::get('/subscribed-channels', 'UserController@subscribed_channels')->name('channels.subscribed');


    // Live videos

    Route::post('broadcast', 'UserController@broadcast')->name('live_video.broadcast');

    Route::get('broadcasting', 'UserController@broadcasting')->name('live_video.start_broadcasting');

    Route::get('stop-streaming', 'UserController@stop_streaming')->name('live_video.stop_streaming');

    Route::post('get_viewer_cnt','UserController@get_viewer_cnt')->name('live_video.get_viewer_cnt');

    Route::post('add/watch_count', 'UserController@watch_count')->name('add.watch_count');


    Route::post('/partialVideos', 'UserController@partialVideos')->name('video.get_videos');

    Route::post('/payment_mgmt_videos', 'UserController@payment_mgmt_videos')->name('video.payment_mgmt_videos');

    Route::get('invoice', 'UserController@invoice')->name('subscription.invoice');

    Route::get('ppv-invoice/{id}', 'UserController@ppv_invoice')->name('subscription.ppv_invoice');

    Route::get('subscription-type/{id}', 'UserController@pay_per_view')->name('subscription.pay_per_view');

    Route::get('pay-per-videos', 'UserController@payper_videos')->name('pay-per-videos');

    Route::post('payment-type/{id}', 'UserController@payment_type')->name('payment-type');

    Route::post('subscription/payment', 'UserController@subscription_payment')->name('subscription.payment');

    Route::get('subscription-success', 'UserController@payment_success')->name('subscription.success');

    Route::get('video-success/{id}', 'UserController@video_success')->name('video.success');

    Route::get('mychannels/list', 'UserController@my_channels')->name('channel.mychannel');

    Route::post('/forgot/password', 'UserController@forgot_password')->name('forgot.password');

    Route::get('subscription/history', 'UserController@subscription_history')->name('subscription.history');

    Route::get('ppv/history', 'UserController@ppv_history')->name('ppv.history');

    Route::get('live/history', 'UserController@live_history')->name('live.history');

    Route::post('/live/videos/mgmt', 'UserController@live_mgmt_videos')->name('live.video.mgmt');

    Route::get('/single/live/video/{id?}' , 'UserController@single_custom_live_video')->name('custom.live.view');

    Route::get('/custom/live/videos' , 'UserController@custom_live_videos')->name('custom.live.index');


});

Route::group(['prefix' => 'userApi'], function(){

        // Live Urls
    
    Route::post('get_live_url', 'UserApiController@get_live_url');

    Route::post('save_vod', 'UserApiController@save_vod');

    Route::post('live_videos', 'UserApiController@live_videos');

    Route::post('save_live_video', 'UserApiController@save_live_video');

    Route::post('/live_video', 'UserApiController@live_video');

    Route::post('save_chat', 'UserApiController@save_chat');

    Route::post('video_subscription', 'UserApiController@video_subscription');

    Route::post('get_viewers', 'UserApiController@get_viewers');

    Route::post('peerProfile', 'UserApiController@peerProfile');

    Route::post('checkVideoStreaming', 'UserApiController@checkVideoStreaming');

    Route::post('close_streaming', 'UserApiController@close_streaming');

    Route::post('stripe_payment_video', 'UserApiController@stripe_payment_video');

    Route::post('ppv_paypal', 'UserApiController@ppv_paypal');

    

    Route::post('/watch_count', 'UserController@watch_count');

    Route::post('/register','UserApiController@register');
    
    Route::post('/login','UserApiController@login');

    Route::get('/userDetails','UserApiController@user_details');

    Route::get('/userDetails','UserApiController@user_details');

    Route::post('/updateProfile', 'UserApiController@update_profile');

    Route::post('/forgotpassword', 'UserApiController@forgot_password');

    Route::post('/changePassword', 'UserApiController@change_password');

    Route::post('/deleteAccount', 'UserApiController@delete_account');

    Route::post('/settings', 'UserApiController@settings');

    // Videos and home

    Route::post('/home' , 'UserApiController@home');

    Route::post('/trending' , 'UserApiController@trending');
    
   // Route::post('/common' , 'UserApiController@common');

    Route::post('/single_video' , 'UserApiController@single_video');
    
   // Route::post('/singleVideo' , 'UserApiController@getSingleVideo');

    Route::post('/searchVideo' , 'UserApiController@search_video')->name('search-video');

    Route::post('/channel_videos', 'UserApiController@get_channel_videos');


    // Rating and Reviews

    Route::post('/userRating', 'UserApiController@user_rating');

    // Wish List

    Route::post('/addWishlist', 'UserApiController@add_wishlist');

    Route::post('/getWishlist', 'UserApiController@get_wishlist');

    Route::post('/deleteWishlist', 'UserApiController@delete_wishlist');

    // History

    Route::post('/addHistory', 'UserApiController@add_history');

    Route::post('getHistory', 'UserApiController@get_history');

    Route::post('/deleteHistory', 'UserApiController@delete_history');

    Route::get('/clearHistory', 'UserApiController@clear_history');

    // Index

    Route::post('/index', 'UserApiController@index');

    //Route::post('/redeems/list', 'UserApiController@redeems');

    // Route::post('/send_redeem_request', 'UserApiController@send_redeem_request');


    Route::post('/like_video', 'UserApiController@likeVideo');

    Route::post('/dis_like_video', 'UserApiController@disLikeVideo');

    // Cards 

    Route::post('card_details', 'UserApiController@card_details');

    Route::post('payment_card_add', 'UserApiController@payment_card_add');

    Route::post('default_card', 'UserApiController@default_card');

    Route::post('delete_card', 'UserApiController@delete_card');

    Route::post('/stripe_payment', 'UserApiController@stripe_payment');
    

    // SubScriptions 

    Route::post('subscription_plans', 'UserApiController@subscription_plans');

    Route::post('subscribedPlans', 'UserApiController@subscribedPlans');

    Route::post('pay_now', 'UserApiController@pay_now');

    Route::post('/my_channels', 'UserApiController@my_channels');

    Route::post('/mychannel/list', 'UserApiController@user_channel_list');

    Route::post('subscribe_channel', 'UserApiController@subscribe_channel');

    Route::post('unsubscribe_channel', 'UserApiController@unsubscribe_channel');

    Route::post('subscribed_channels', 'UserApiController@subscribed_channels');

    Route::post('/add_spam', 'UserApiController@add_spam');

    Route::get('/spam-reasons', 'UserApiController@reasons');

    Route::post('remove_spam', 'UserApiController@remove_spam');

    Route::post('spam_videos', 'UserApiController@spam_videos_list');


    Route::post('channel/create', 'UserApiController@create_channel');

    Route::post('ppv_list', 'UserApiController@ppv_list');

    Route::post('/redeems/list', 'UserApiController@redeems');

    Route::post('redeem/request/list', 'UserApiController@redeem_request_list');

    Route::post('redeems/request', 'UserApiController@send_redeem_request');

    Route::post('redeem/request/cancel', 'UserApiController@redeem_request_cancel');

    Route::post('paypal_ppv', 'UserApiController@paypal_ppv');

    Route::post('stripe_ppv', 'UserApiController@stripe_ppv');

    Route::post('channel/edit', 'UserApiController@channel_edit');

    Route::post('channel/delete', 'UserApiController@channel_delete');

    Route::post('/video/erase-streaming', 'UserApiController@erase_streaming');

});
